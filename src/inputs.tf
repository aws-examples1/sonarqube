variable "admin_password" {
  type        = string
  description = "SonarQube admin user password."
  default     = "admin"
}

variable "CI_COMMIT_SHORT_SHA" {
  type         = string
  description  = "The first eight characters of the commit revision for which project is built."
}

variable "CI_JOB_ID" {
  type        = string
  description = "The unique id of the current job that GitLab CI/CD uses internally."
}

variable "CI_PROJECT_PATH_SLUG" {
  type        = string
  description = "Identifies the Gitlab repository that this project came from."
}

variable "certificate_arn" {
  type        = string
  description = "An ARN identifying an AWS Certificate Manager SSL certificate associated with the domain identified by the domain variable."
}

variable "database_password" {
  type        = string
  description = "Assigned to SonarQube database during provisioning.  Will remove and replace with Thycotic or Secrets Manager call in future."
}

variable "domain" {
  type        = string
  description = "Used to retrieve the Route 53 zone where records that need to be resolvable across the AWS and on-premise environments are created."
}

variable "network_account_role" {
  type        = string
  description = "Controls where the external domain name registrations are performed."
}

variable "new_user_default_password" {
  type        = string
  description = "Default password assigned to any local SonarQube users provisioned by this process."
}
