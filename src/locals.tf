locals {
  ami = {
    default                     = "ami-05657d1958cd8c268"
    sandbox                     = "ami-05657d1958cd8c268"
    production                  = "ami-05657d1958cd8c268"
  }
  application                   = "sonar"
  admin_username                = "admin"
  base_hostname                 = "sonarqube"
  base_url                      = "https://${local.hostname}.${var.domain}/"
  cidr_aws_new                  = [
    "10.44.0.0/14"
  ]
  cidr_vdi                      = [
    "10.128.0.0/16"
  ]
  cidr_aws_old                  = [
    "172.50.12.0/24",
    "172.31.0.0/16"
  ]
  cidr_external_vip             = [
    "174.128.46.4/32"
  ]
  cloud_init_complete_filename  = "/opt/sonarqube/logs/access.log"
  database_name                 = "sonarqube"
  database_username             = "sonarqube"
  groups                        = jsondecode(file("${path.module}/files/groups/${terraform.workspace}.json"))
  hostname                      = terraform.workspace == "production" ? local.base_hostname : "${local.base_hostname}-${terraform.workspace}"
  instance_count                = 1
  instance_type                 = "t3.large"
  login_url                     = {
    default     = "https://login.microsoftonline.com/35b766ab-46b9-45a9-858b-356849556fd1/saml2"
    sandbox     = "https://login.microsoftonline.com/35b766ab-46b9-45a9-858b-356849556fd1/saml2"
    production  = "https://login.microsoftonline.com/f7053f18-68f3-4a43-aec4-4902f7820563/saml2"
  }
  permissions                   = flatten([
    for group in local.groups : [
      for permission in group.permissions : {
        identifier = group.identifier
        permission = permission
      }
    ]
  ])
  provider_id                   = {
    default     = "https://sts.windows.net/35b766ab-46b9-45a9-858b-356849556fd1/"
    sandbox     = "https://sts.windows.net/35b766ab-46b9-45a9-858b-356849556fd1/"
    production  = "https://sts.windows.net/f7053f18-68f3-4a43-aec4-4902f7820563/"
  }
  root_block_device_volume_size = 100
  salt_complete_function        = {
    default     = "arn:aws:lambda:us-east-1:142876234577:function:USC1E1003S-SALT-AUTOSIGN-GRAINS-UPDATED-DATA-SOURCE"
    sandbox     = "arn:aws:lambda:us-east-1:013163831747:function:USC1E1003S-SALT-AUTOSIGN-GRAINS-UPDATED-DATA-SOURCE"
    development = "arn:aws:lambda:us-east-1:013163831747:function:USC1E1003D-SALT-AUTOSIGN-GRAINS-UPDATED-DATA-SOURCE"
    qa          = "arn:aws:lambda:us-east-1:013163831747:function:USC1E1003Q-SALT-AUTOSIGN-GRAINS-UPDATED-DATA-SOURCE"
    production  = "arn:aws:lambda:us-east-1:013163831747:function:USC1E1003P-SALT-AUTOSIGN-GRAINS-UPDATED-DATA-SOURCE"
  }
  salt_role                     = "sonarqube"
  salt_uuid_functions           = {
    default     = "arn:aws:lambda:us-east-1:142876234577:function:USC1E1003S-SALT-AUTOSIGN-GRAINS-RESERVATION"
    sandbox     = "arn:aws:lambda:us-east-1:013163831747:function:USC1E1003S-SALT-AUTOSIGN-GRAINS-RESERVATION"
    development = "arn:aws:lambda:us-east-1:013163831747:function:USC1E1003D-SALT-AUTOSIGN-GRAINS-RESERVATION"
    qa          = "arn:aws:lambda:us-east-1:013163831747:function:USC1E1003Q-SALT-AUTOSIGN-GRAINS-RESERVATION"
    production  = "arn:aws:lambda:us-east-1:013163831747:function:USC1E1003P-SALT-AUTOSIGN-GRAINS-RESERVATION"
  }
  salt_uuid_update_function     = {
    default     = "arn:aws:lambda:us-east-1:142876234577:function:USC1E1003S-SALT-AUTOSIGN-GRAINS-RESERVATION-UPDATE"
    sandbox     = "arn:aws:lambda:us-east-1:013163831747:function:USC1E1003S-SALT-AUTOSIGN-GRAINS-RESERVATION-UPDATE"
    development = "arn:aws:lambda:us-east-1:013163831747:function:USC1E1003D-SALT-AUTOSIGN-GRAINS-RESERVATION-UPDATE"
    qa          = "arn:aws:lambda:us-east-1:013163831747:function:USC1E1003Q-SALT-AUTOSIGN-GRAINS-RESERVATION-UPDATE"
    production  = "arn:aws:lambda:us-east-1:013163831747:function:USC1E1003P-SALT-AUTOSIGN-GRAINS-RESERVATION-UPDATE"
  }
  saltmaster                    = "saltmaster-${terraform.workspace}.aws.gafg.com"
  settings                      = [
    {
      key = "sonar.core.serverBaseURL"
      value = local.base_url
    },
    {
      key = "sonar.auth.saml.applicationId"
      value = local.base_url
    },
    {
      key = "sonar.auth.saml.providerName"
      value = "Azure AD"
    },
    {
      key = "sonar.auth.saml.providerId"
      value = local.provider_id[terraform.workspace]
    },
    {
      key = "sonar.auth.saml.loginUrl"
      value = local.login_url[terraform.workspace]
    },
    {
      key = "sonar.auth.saml.certificate.secured"
      value = file("${path.module}/files/public_certificates/${terraform.workspace}.cer")
    },
    {
      key = "sonar.auth.saml.user.login"
      value = "login"
    },
    {
      key = "sonar.auth.saml.user.name"
      value = "name"
    },
    {
      key = "sonar.auth.saml.user.email"
      value = "email"
    },
    {
      key = "sonar.auth.saml.group.name"
      value = "groups"
    },
    {
      key = " sonar.auth.saml.enabled"
      value = true
    }
  ]
  tags                          = {
    "gafg:account"     = module.data.account_alias
    "gafg:application" = local.application
    "gafg:billing"     = "cloudops"
    "gafg:commit"      = var.CI_COMMIT_SHORT_SHA
    "gafg:data_class"  = "internal"
    "gafg:environment" = terraform.workspace
    "gafg:repo"        = var.CI_PROJECT_PATH_SLUG
  }
  users                         = flatten([
    for group in local.groups : [
      for user in group.users : {
        identifier = group.identifier
        login      = user.login
        name       = user.name
        email      = user.email
      }
    ]
  ])
  workspace_iam_roles           = {
    default    = "arn:aws:iam::013163831747:role/GitlabRunnerRole"
    sandbox    = "arn:aws:iam::013163831747:role/GitlabRunnerRole"
    production = "arn:aws:iam::013163831747:role/GitlabRunnerRole"
  }
}
