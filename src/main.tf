###############################################################################
# Provider Declarations
###############################################################################
provider "aws" {
  version = "~> 2.51"
  region  = "us-east-1"

  assume_role {
    role_arn = local.workspace_iam_roles[terraform.workspace]
  }
}

provider "aws" {
  alias   = "network"
  version = "~> 2.51"
  region  = "us-east-1"

  assume_role {
    role_arn = var.network_account_role
  }
}

provider "null" {
  version = "~> 2.1"
}

###############################################################################
# Values Pulled from Primary AWS Account
###############################################################################
module "data" {
  source = "./modules/data"
}

###############################################################################
# Values Pulled from AWS Network Account
###############################################################################
data "aws_route53_zone" "default" {
  provider = aws.network

  name         = var.domain
  private_zone = false
}

###############################################################################
# Base Naming Convention
###############################################################################
module "name" {
  source = "./modules/resource_name"

  application   = local.application
  cloud         = "AWS"
  environment   = terraform.workspace
  location      = module.data.current_region
  os            = "linux"
  purpose       = "application"
  suffix        = "-${local.application}"
}

###############################################################################
# Server Role
###############################################################################
resource "aws_iam_role" "default" {
  name               = module.name.resource_base_name
  assume_role_policy = file("./files/iam_policies/assume_role_policy.json")
  tags               = local.tags
}

resource "aws_iam_role_policy" "default" {
  name   = module.name.resource_base_name
  role   = aws_iam_role.default.id
  policy = templatefile("./files/iam_policies/policy.json", { SNS_ARN: aws_sns_topic.default.arn })
}

###############################################################################
# Provision SonarQube Database
###############################################################################
resource "aws_security_group" "database" {
  name   = "${module.name.resource_base_name}RDS"
  vpc_id = module.data.vpc_id

  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = concat(local.cidr_vdi, [module.data.vpc_cidr_block])
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags   = local.tags
}

resource "aws_db_subnet_group" "default" {
  name       = lower(module.name.resource_base_name)
  subnet_ids = module.data.private_subnet_ids
  tags       = local.tags
}

resource "aws_rds_cluster" "default" {
  cluster_identifier        = lower(module.name.resource_base_name)
  database_name             = local.database_name
  db_subnet_group_name      = aws_db_subnet_group.default.name
  engine                    = "aurora-postgresql"
  engine_mode               = "serverless"
  skip_final_snapshot       = terraform.workspace == "production"  ? false : true
  final_snapshot_identifier = terraform.workspace == "production" ? module.name.resource_base_name : null
  master_password           = var.database_password
  master_username           = local.database_username
  vpc_security_group_ids    = [aws_security_group.database.id]
}

###############################################################################
# SNS Topic for Instance Alerts
###############################################################################
resource "aws_sns_topic" "default" {
  name = module.name.resource_base_name
}

###############################################################################
# Provision SonarQube Instance(s)
###############################################################################
module "user_data_template_values" {
  source = "./modules/user_data_template_values"

  aws_account_alias            = module.data.account_alias
  aws_region                   = module.data.current_region
  cloud_init_complete_filename = local.cloud_init_complete_filename
  database_username            = local.database_username
  database_password            = var.database_password
  database_url                 = "jdbc:postgresql://${aws_rds_cluster.default.endpoint}/${local.database_name}"
  instance_count               = local.instance_count
  salt_role                    = local.salt_role
  salt_uuid_function           = local.salt_uuid_functions[terraform.workspace]
  salt_uuid_update_function    = local.salt_uuid_update_function[terraform.workspace]
  saltmaster                   = local.saltmaster
  server_base_name             = module.name.server_base_name
  sns_arn                      = aws_sns_topic.default.arn
}

module "sonarqube" {
  source = "./modules/aws_instance"

  ami                               = local.ami[terraform.workspace]
  associate_public_ip_address       = false
  availability_zones                = module.data.private_subnet_availability_zones
  ebs_optimized                     = false
  egress_cidr_rules                 = [
    {
      from_port = 0
      to_port   = 0
      protocol  = -1
      cidr_blocks = ["0.0.0.0/0"]
      description = "Allow unconstrained access to the Internat."
    }
  ]
  ingress_cidr_rules                = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "icmp"
      cidr_blocks = concat(local.cidr_aws_new, local.cidr_vdi, local.cidr_aws_old, [module.data.vpc_cidr_block], local.cidr_external_vip)
      description = "Allow ping from all internal IP address ranges."
    },
    {
      from_port   = 9000
      to_port     = 9000
      protocol    = "tcp"
      cidr_blocks = concat(local.cidr_aws_new, local.cidr_vdi, local.cidr_aws_old, [module.data.vpc_cidr_block], local.cidr_external_vip)
      description = "Temporary rule to troubleshoot connectivity."
    }
  ]
  ingress_group_rules               = [
    {
      from_port       = 9000
      to_port         = 9000
      protocol        = "tcp"
      security_groups = [module.load_balancer.security_group_id]
      description     = "Allow HTTP communication from the load balancer."
    }
  ]
  instance_count                    = local.instance_count
  instance_type                     = local.instance_type
  base_resource_name                = module.name.resource_base_name
  base_server_name                  = module.name.server_base_name
  iam_role                          = aws_iam_role.default.name
  root_block_device_volume_size     = local.root_block_device_volume_size
  subnet_ids                        = module.data.private_subnet_ids
  tags                              = local.tags
  user_data_template_file           = "${path.module}/files/templates/user_data.tpl"
  user_data_template_values         = module.user_data_template_values.value
  vpc_id                            = module.data.vpc_id
}

###############################################################################
# Setup Load Balancer to Perform SSL Termination
###############################################################################
module "load_balancer" {
  source = "./modules/load_balancer"

  certificate_arn   = var.certificate_arn
  name              = module.name.resource_base_name
  ingress_cidrs     = concat(local.cidr_aws_new, local.cidr_vdi, local.cidr_aws_old, [module.data.vpc_cidr_block], local.cidr_external_vip)
  private           = true
  subnet_ids        = module.data.private_subnet_ids
  tags              = local.tags
  vpc_id            = module.data.vpc_id
}

module "load_balancer_attachment" {
  source = "./modules/load_balancer_attachment"

  hostname     = local.hostname
  listener_arn = module.load_balancer.https_listener_arn
  instance_ids = module.sonarqube.instance_ids
  port         = "9000"
  protocol     = "HTTP"
  priority     = 1
  vpc_id       = module.data.vpc_id
}

###############################################################################
# Register Entrprise-Wide DNS Name for SonarQube Load Balancer
###############################################################################
resource "aws_route53_record" "default" {
  provider = aws.network

  zone_id = data.aws_route53_zone.default.id
  name = local.hostname
  type = "CNAME"
  ttl  = 5
  records = [module.load_balancer.dns_name]
}

data "aws_lambda_invocation" "sonarqube_ready" {
  count = local.instance_count

  function_name = local.salt_complete_function[terraform.workspace]
  input         =<<JSON
{
  "instance_id": "${module.sonarqube.instance_ids[count.index]}"
}
JSON

  depends_on = [
    module.sonarqube.wait_on
  ]
}

# ###############################################################################
# Configure SAML 2.0
# ###############################################################################
resource "null_resource" "settings" {
  count = length(local.settings)

  triggers = {
    value        = urlencode(local.settings[count.index].value)
    instance_ids = join(",", module.sonarqube.instance_ids)
  }

  provisioner "local-exec" {
    command     = "bash ${path.module}/scripts/set-property.sh"

    environment = {
      USERNAME  = local.admin_username
      PASSWORD  = var.admin_password
      ENDPOINT  = local.base_url
      KEY       = local.settings[count.index].key
      VALUE     = urlencode(local.settings[count.index].value)
    }
  }

  depends_on = [
    module.load_balancer_attachment.wait_on,
    aws_route53_record.default,
    data.aws_lambda_invocation.sonarqube_ready
  ]
}

# ###############################################################################
# Configure Groups
# ###############################################################################
resource "null_resource" "groups" {
  count = length(local.groups)

  triggers = {
    value        = local.groups[count.index].identifier
    instance_ids = join(",", module.sonarqube.instance_ids)
  }

  provisioner "local-exec" {
    command     = "bash ${path.module}/scripts/create-group.sh"

    environment = {
      USERNAME    = local.admin_username
      PASSWORD    = var.admin_password
      ENDPOINT    = local.base_url
      NAME        = local.groups[count.index].identifier
      DESCRIPTION = urlencode(local.groups[count.index].description)
    }
  }

  depends_on = [
    module.load_balancer_attachment.wait_on,
    aws_route53_record.default,
    data.aws_lambda_invocation.sonarqube_ready
  ]
}

# ###############################################################################
# Configure Group Permissions
# ###############################################################################
resource "null_resource" "permissions" {
  count = length(local.permissions)

  triggers = {
    group_name      = local.permissions[count.index].identifier
    permission_name = local.permissions[count.index].permission
    instance_ids    = join(",", module.sonarqube.instance_ids)
  }

  provisioner "local-exec" {
    command     = "bash ${path.module}/scripts/add-permission.sh"

    environment  = {
      USERNAME        = local.admin_username
      PASSWORD        = var.admin_password
      ENDPOINT        = local.base_url
      GROUP_NAME      = local.permissions[count.index].identifier
      PERMISSION_NAME = local.permissions[count.index].permission
    }
  }

  depends_on = [
    null_resource.groups
  ]
}

# ###############################################################################
# Configure Local Users
# ###############################################################################
resource "null_resource" "users" {
  count = length(local.users)

  triggers = {
    name         = local.users[count.index].name
    email        = local.users[count.index].email
    login        = local.users[count.index].login
    instance_ids = join(",", module.sonarqube.instance_ids)
  }

  provisioner "local-exec" {
    command     = "bash ${path.module}/scripts/add-user.sh"

    environment = {
      USERNAME       = local.admin_username
      PASSWORD       = var.admin_password
      ENDPOINT       = local.base_url
      NAME           = local.users[count.index].name
      EMAIL          = local.users[count.index].email
      LOGIN          = local.users[count.index].login
      LOGIN_PASSWORD = var.new_user_default_password
    }
  }

  depends_on = [
    module.load_balancer_attachment.wait_on,
    aws_route53_record.default,
    data.aws_lambda_invocation.sonarqube_ready
  ]
}

# ###############################################################################
# Configure Local User Permissions
# ###############################################################################
resource "null_resource" "user_permissions" {
  count = length(local.users)

  triggers = {
    group_name   = local.users[count.index].identifier
    login        = local.users[count.index].login
    instance_ids = join(",", module.sonarqube.instance_ids)
  }

  provisioner "local-exec" {
    command     = "bash ${path.module}/scripts/add-user-to-group.sh"

    environment = {
      USERNAME   = local.admin_username
      PASSWORD   = var.admin_password
      ENDPOINT   = local.base_url
      GROUP_NAME = local.users[count.index].identifier
      LOGIN      = local.users[count.index].login
    }
  }

  depends_on = [
    null_resource.groups,
    null_resource.users
  ]
}
