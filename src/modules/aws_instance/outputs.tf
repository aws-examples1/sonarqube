output "aws_public_dns" {
  description = "The public DNS name assigned to the instance by AWS. For EC2-VPC, this is only available if you've enabled DNS hostnames for your VPC"
  value       = aws_instance.default.*.public_dns
}

output "instance_ids" {
  description = "The AWS-assigned identifier for each provisioned instance."
  value = aws_instance.default.*.id
}

output "private_dns" {
  description = "The private DNS name assigned to the instance. Can only be used inside the Amazon EC2, and only available if you've enabled DNS hostnames for your VPC."
  value       = aws_instance.default.*.private_dns
}

output "private_ips" {
  description = "These are the private IP addresses assigned to the provisioned instances."
  value       = aws_instance.default.*.private_ip
}

output "public_ips" {
  description = "These are the public IP addresses assigned to the provisioned instances if applicable."
  value = aws_instance.default.*.public_ip
}

output "wait_on" {
  value = "EC2 Instances Deployed: ${var.base_server_name}"

  depends_on = [
    aws_instance.default
  ]
}
