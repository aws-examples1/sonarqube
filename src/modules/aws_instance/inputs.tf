variable  "ami" {
  type        = string
  description = "The AMI to use for the instance."
}

variable "associate_public_ip_address" {
  type        = bool
  description = "Associate a public ip address with an instance in a VPC."
}

variable "availability_zones" {
  type        = list
  description = "The AZ to start the instance in. If instance_count is greater than 1 then each subsequent instance will be assigned to a subsequent availability zone."
}

variable "base_resource_name" {
  type        = string
  description = "This value will serve as the name prefix for each resource provisioned."
}

variable "base_server_name" {
  type        = string
  description = "This value will serve as the name prefix for each instance provisioned."
}

variable "ebs_block_device" {
  type        = list(map(string))
  description = "Additional EBS block devices to attach to the instance. Block device configurations only apply on resource creation."
  default     = []
}

variable "ebs_optimized" {
  type        = bool
  description = "If true, the launched EC2 instance will be EBS-optimized."
  default     = false
}

variable "egress_cidr_rules" {
  type        = list
  description = "Egress security group rules to be applied to each instance provisioned."
  default     = []
}

variable "egress_group_rules" {
  type        = list
  description = "Egress security group rules to be applied to each instance provisioned."
  default     = []
}

variable "iam_role" {
  type        = string
  description = "IAM role to be assigned to instance profile."
}

variable "ingress_cidr_rules" {
  type        = list
  description = "Ingress security group rules to be applied to each instance provisioned."
  default     = []
}

variable "ingress_group_rules" {
  type        = list
  description = "Ingress security group rules to be applied to each instance provisioned."
  default     = []
}

variable "instance_count" {
  type        = number
  description = "Defines the number of instances to be provisioned."
}


variable "instance_type" {
  type        = string
  description = "The type of instance to start. Updates to this field will trigger a stop/start of the EC2 instance."
}

variable "key_name" {
  type        = string
  description = "The key name of the Key Pair to use to SSH into the instance."
  default     = ""
}

variable "placement_group" {
  type        = string
  description = "The Placement Group to start the instance in"
  default     = ""
}

variable "root_block_device_volume_size" {
  type        = number
  description = "The size of the volume in gibibytes (GiB)."
  default     = 8
}

variable "subnet_ids" {
  type        = list
  description = "A list of VPC subnet IDs to launch instances in. A round-robin approach is used to assign subnets to instances."
}

variable "tags" {
  type        = map(string)
  description = "A mapping of tags to assign to the resource"
  default     = {}
}

variable "user_data_template_file" {
  type        = string
  description = "This points to a template file that will be combined with instance-specific maps contained in the user_data_template_values variable and passed as instance user_data."
  default     = ""
}

variable "user_data_template_values" {
  type        = list
  description = "List containing instance-specific maps to be combined with the contents of the user_data_template_file variable and passed as instance user_data."
  default     = []
}

variable "vpc_id" {
  type        = string
  description = "This is the AWS-assigned identifier for the VPC to be associated with the provisioned security group."
}

variable "wait_on" {
  type = list
  default = []
  description = "This module will wait for any resources listed to complete before it starts executing."
}
