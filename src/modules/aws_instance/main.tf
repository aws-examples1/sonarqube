###############################################################################
# Static Values Used Throughout
###############################################################################
locals {
  root_block_device_volume_type = "gp2"
}

###############################################################################
# Used to Enable Inter-Module Dependency
###############################################################################
resource "null_resource" "waited_on" {
  count = length(var.wait_on)

  provisioner "local-exec" {
    command = "echo Dependency ${count.index + 1} of ${length(var.wait_on)} Resolved"
  }
}

###############################################################################
# Security Group for EC2 Instances
###############################################################################
resource "aws_security_group" "default" {
  lifecycle {
    create_before_destroy = true
  }

  name                   = "${var.base_resource_name}EC2"
  vpc_id                 = var.vpc_id

  dynamic "ingress" {
    for_each = var.ingress_cidr_rules
    content {
      from_port       = ingress.value.from_port
      to_port         = ingress.value.to_port
      protocol        = ingress.value.protocol
      cidr_blocks     = lookup(ingress.value, "cidr_blocks", null)
      description     = lookup(ingress.value, "description", null)
    }
  }

  dynamic "ingress" {
    for_each = var.ingress_group_rules
    content {
      from_port       = ingress.value.from_port
      to_port         = ingress.value.to_port
      protocol        = ingress.value.protocol
      security_groups = lookup(ingress.value, "security_groups", null)
      description     = lookup(ingress.value, "description", null)
    }
  }

  dynamic "egress" {
    for_each = var.egress_cidr_rules
    content {
      from_port       = egress.value.from_port
      to_port         = egress.value.to_port
      protocol        = egress.value.protocol
      cidr_blocks     = lookup(egress.value, "cidr_blocks", null)
      description     = lookup(egress.value, "description", null)
    }
  }

  dynamic "egress" {
    for_each = var.egress_group_rules
    content {
      from_port       = egress.value.from_port
      to_port         = egress.value.to_port
      protocol        = egress.value.protocol
      security_groups = lookup(egress.value, "security_groups", null)
      description     = lookup(egress.value, "description", null)
    }
  }

  tags = var.tags

  depends_on = [
    null_resource.waited_on
  ]
}

###############################################################################
# Instances
###############################################################################
resource "aws_iam_instance_profile" "default" {
  name = "${var.base_resource_name}PROFILE"
  role = var.iam_role

  depends_on = [
    null_resource.waited_on
  ]
}

resource "aws_instance" "default" {
  count                       = var.instance_count

  ami                         = var.ami
  associate_public_ip_address = var.associate_public_ip_address
  availability_zone           = element(var.availability_zones, count.index + 1 % length(var.availability_zones))
  ebs_optimized               = var.ebs_optimized
  iam_instance_profile        = aws_iam_instance_profile.default.name
  instance_type               = var.instance_type
  key_name                    = var.key_name
  placement_group             = var.placement_group
  subnet_id                   = element(var.subnet_ids, count.index + 1 % length(var.subnet_ids))
  user_data                   = var.user_data_template_file != "" ? templatefile(var.user_data_template_file, length(var.user_data_template_values) > count.index ? var.user_data_template_values[count.index] : {}) : null
  vpc_security_group_ids      = [aws_security_group.default.id]

  root_block_device {
    delete_on_termination = true
    volume_size           = var.root_block_device_volume_size
    volume_type           = local.root_block_device_volume_type
  }

  dynamic "ebs_block_device" {
    for_each = var.ebs_block_device
    content {
      delete_on_termination = lookup(ebs_block_device.value, "delete_on_termination", null)
      device_name           = ebs_block_device.value.device_name
      iops                  = lookup(ebs_block_device.value, "iops", null)
      snapshot_id           = lookup(ebs_block_device.value, "snapshot_id", null)
      volume_size           = lookup(ebs_block_device.value, "volume_size", null)
      volume_type           = lookup(ebs_block_device.value, "volume_type", null)
    }
  }

  tags = merge(
    {
      Name = format("${var.base_server_name}%02d", count.index + 1)
    },
    var.tags
  )
}
