output "value" {
  value = [
     for i in range(var.instance_count) : {
        aws_account_alias            = var.aws_account_alias
        aws_region                   = var.aws_region
        cloud_init_complete_filename = var.cloud_init_complete_filename
        database_username            = var.database_username
        database_password            = var.database_password
        database_url                 = var.database_url
        salt_role                    = var.salt_role
        salt_uuid_function           = var.salt_uuid_function
        salt_uuid_update_function    = var.salt_uuid_update_function
        saltmaster                   = var.saltmaster
        salt_minion_id               = "${format("${var.server_base_name}%02d", i + 1)}"
        sns_arn                      = var.sns_arn
      }
  ]
}
