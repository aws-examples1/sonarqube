variable "aws_account_alias" {
  type = string
}

variable "aws_region" {
  type = string
}

variable "cloud_init_complete_filename" {
  type = string
}

variable "database_password" {
  type = string
}

variable "database_url" {
  type = string
}

variable "database_username" {
  type = string
}

variable "instance_count" {
  type = string
}

variable "salt_role" {
  type = string
}

variable "salt_uuid_function" {
  type = string
}

variable "salt_uuid_update_function" {
  type = string
}

variable "saltmaster" {
  type = string
}

variable "server_base_name" {
  type = string
}

variable "sns_arn" {
  type = string
}
