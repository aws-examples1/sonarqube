output "dns_name" {
  value = aws_lb.default.dns_name
}

output "https_listener_arn" {
  value = aws_lb_listener.https.arn
}

output "security_group_id" {
  value = aws_security_group.default.id
}

output "wait_on" {
  value = "Load Balancer Provisioned: ${var.name}"

  depends_on = [
    aws_lb_listener.https,
    aws_lb_listener.http
  ]
}
