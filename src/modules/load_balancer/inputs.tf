variable "ingress_cidrs" {
  type        = list
  description = "List of CIDR blocks that should have access to all ports exposed by the load balancer."
}

variable "name" {
  type        = string
  description = "This value will serve as the name for each resource provisioned."
}

variable "certificate_arn" {
  type        = string
  description = "An ARN identifying an AWS Certificate Manager SSL certificate."
}

variable "private" {
  type        = string
  description = "Determines whether or not the load balancer provisioned will be private."
}

variable "subnet_ids" {
  type        = list
  description = "List of public subnets to attach the load balancer to."
}

variable "tags" {
  description = "A mapping of tags to assign to the resource"
  type        = map(string)
  default     = {}
}

variable "vpc_id" {
  type        = string
  description = "This is the AWS-assigned identifier for the VPC to be associated with the provisioned security group."
}
