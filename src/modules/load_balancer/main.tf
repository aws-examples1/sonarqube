###############################################################################
# Static Values Used Throughout
###############################################################################
locals {
  egress_rules = [
    {
      from_port = "0"
      to_port = "0"
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }
  ]
  ingress_rules = [
    {
      from_port = "443"
      to_port = "443"
      protocol = "tcp"
      cidr_blocks = var.ingress_cidrs
    },
    {
      from_port = "80"
      to_port = "80"
      protocol = "tcp"
      cidr_blocks = var.ingress_cidrs
    }
  ]
}

###############################################################################
# Security Group for Load Balancer
###############################################################################
resource "aws_security_group" "default" {
  name                   = "${var.name}ALB"
  vpc_id                 = var.vpc_id

  dynamic "ingress" {
    for_each = local.ingress_rules
    content {
      from_port   = ingress.value.from_port
      to_port     = ingress.value.to_port
      protocol    = ingress.value.protocol
      cidr_blocks = lookup(ingress.value, "cidr_blocks", null)
    }
  }

  dynamic "egress" {
    for_each = local.egress_rules
    content {
      from_port   = egress.value.from_port
      to_port     = egress.value.to_port
      protocol    = egress.value.protocol
      cidr_blocks = lookup(egress.value, "cidr_blocks", null)
    }
  }

  tags = var.tags
}

###############################################################################
# Main
###############################################################################
resource "aws_lb" "default" {
  name               = var.name
  internal           = var.private
  load_balancer_type = "application"
  security_groups    = [aws_security_group.default.id]
  subnets            = var.subnet_ids
}

resource "aws_lb_listener" "https" {
  load_balancer_arn = aws_lb.default.id
  port              = 443
  protocol          = "HTTPS"
  certificate_arn   = var.certificate_arn

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/html"
      message_body = file("${path.module}/files/404.html")
      status_code  = 404
    }
  }
}

resource "aws_lb_listener" "http" {
  load_balancer_arn = aws_lb.default.id
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

