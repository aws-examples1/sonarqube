output "wait_on" {
  value = "Load Balancer Attachment Configured: ${var.hostname}"

  depends_on = [
    aws_lb_listener_rule.default,
    aws_lb_target_group_attachment.default
  ]
}
