variable "hostname" {
  type        = string
  description = "This is the hostname that will used for matching the host header and forwarding the request to the specified port and listener_ids."
}

variable "port" {
  type        = number
  description = "The port that the target service is listening on."
}
variable "listener_arn" {
  type        = string
  description = "Amazon resource number identifying the load balancer listener through which requests targeting the specified service are expected."
}

variable "instance_ids" {
  type        = list
  description = "Identifies the EC2 instances where the target service is running."
}


variable "priority" {
  type        = number
  description = "Determines the order tht rule identifying this target service should be evaluated in by the load balancer."
}


variable "protocol" {
  type        = string
  description = "The protocol that the target service supports."
}

variable "vpc_id" {
  type        = string
  description = "The virtual private network where the target service is running."
}
