resource "aws_lb_target_group" "default" {
   lifecycle {
    create_before_destroy = true
  }

  port     = var.port
  protocol = var.protocol
  vpc_id   = var.vpc_id
}

resource "aws_lb_listener_rule" "default" {
  listener_arn = var.listener_arn
  priority     = var.priority

  action{
    type = "forward"
    target_group_arn = aws_lb_target_group.default.arn
  }

  condition {
    host_header {
      values = ["${var.hostname}.*"]
    }
  }
}

resource "aws_lb_target_group_attachment" "default" {
  count = length(var.instance_ids)

  target_group_arn = aws_lb_target_group.default.arn
  target_id        = var.instance_ids[count.index]
  port             = var.port
}
