output "account_alias" {
  value = data.aws_iam_account_alias.default.account_alias
}

output "account_id" {
  value = data.aws_caller_identity.default.account_id
}

output "current_region" {
  value = data.aws_region.default.name
}

output "private_subnet_availability_zones" {
  value = [for s in data.aws_subnet.private : s.availability_zone]
}

output "private_subnet_cidrs" {
  value = [for s in data.aws_subnet.private : s.cidr_block]
}

output "private_subnet_ids" {
  value = [for s in data.aws_subnet.private : s.id]
}

output "public_subnet_availability_zones" {
  value = [for s in data.aws_subnet.public : s.availability_zone]
}

output "public_subnet_cidrs" {
  value = [for s in data.aws_subnet.public : s.cidr_block]
}

output "public_subnet_ids" {
   value = [for s in data.aws_subnet.public : s.id]
}

output "vpc_cidr_block" {
  value = data.aws_vpc.default.cidr_block
}

output "vpc_id" {
  value = data.aws_vpc.default.id
}
