locals {
 private_subnet_ids = [
    "subnet-048fbdf55a9388543",
    "subnet-030abc2166db8f796"
  ]
}


data "aws_caller_identity" "default" {}

data "aws_iam_account_alias" "default" {}

data "aws_region" "default" {}

data "aws_vpc" "default" {}

# Need Better Private Filtering Option
# data "aws_subnet_ids" "private" {
#   vpc_id = data.aws_vpc.default.id

#   tags = {
#     "gafg:type" = "private"
#   }
# }

data "aws_subnet_ids" "public" {
  vpc_id = data.aws_vpc.default.id

  tags = {
    "gafg:type" = "public"
  }
}

data "aws_subnet" "private" {
 for_each = toset(local.private_subnet_ids)
 id       = each.value
}

data "aws_subnet" "public" {
 for_each = data.aws_subnet_ids.public.ids
 id       = each.value
}
