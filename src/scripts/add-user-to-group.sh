#!/bin/bash

set -e pipefail

# #############################################################################
# Validate Inputs
# #############################################################################
error=0
if [ -z "${USERNAME}" ]; then
  echo "The variable USERNAME must be provided!"
  ERROR=$((${ERROR}+1))
fi

if [ -z "${PASSWORD}" ]; then
  echo "The variable PASSWORD must be provided!"
  ERROR=$((${ERROR}+1))
fi

if [ -z "${ENDPOINT}" ]; then
  echo "The variable ENDPOINT must be provided!"
  ERROR=$((${ENDPOINT}+1))
fi

if [ -z "${GROUP_NAME}" ]; then
  echo "The variable GROUP_NAME must be provided!"
  ERROR=$((${ERROR}+1))
fi

if [ -z "${LOGIN}" ]; then
  echo "The variable LOGIN must be provided!"
  ERROR=$((${ERROR}+1))
fi

if (( ERROR != 0 )); then
  exit ${ERROR}
fi

# #############################################################################
# Send Request
# #############################################################################
LOOP=1
CONTINUE=true
EXIT=0
while [ $CONTINUE = true ]; do
  STATUS=$(curl --output /dev/null --write-out '%{http_code}' --user "${USERNAME}:${PASSWORD}" --data "name=${GROUP_NAME}&login=${LOGIN}" ${ENDPOINT}api/user_groups/add_user)

  if [ $STATUS -eq 204 ]; then
    echo "*** Successfully Added User to Group ***"
    CONTINUE=false
  elif [ $LOOP -ne 15 ]; then
    echo "Error: $STATUS returned from service. Sleeping for 60 seconds and then retrying: $LOOP of 10."
    sleep 60
    LOOP=$((LOOP+1))
  else
    echo "Unable to Add User to Group: $STATUS"
    EXIT=$STATUS
    CONTINUE=false
  fi
done

exit $EXIT
