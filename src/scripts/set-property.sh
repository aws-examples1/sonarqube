#!/bin/bash

set -e pipefail

# #############################################################################
# Validate Inputs
# #############################################################################
error=0
if [ -z "${USERNAME}" ]; then
  echo "The variable USERNAME must be provided!"
  ERROR=$((${ERROR}+1))
fi

if [ -z "${PASSWORD}" ]; then
  echo "The variable PASSWORD must be provided!"
  ERROR=$((${ERROR}+1))
fi

if [ -z "${ENDPOINT}" ]; then
  echo "The variable ENDPOINT must be provided!"
  ERROR=$((${ENDPOINT}+1))
fi

if [ -z "${KEY}" ]; then
  echo "The variable KEY must be provided!"
  ERROR=$((${ERROR}+1))
fi

if [ -z "${VALUE}" ]; then
  echo "The variable VALUE must be provided!"
  ERROR=$((${ERROR}+1))
fi

if (( ERROR != 0 )); then
  exit ${ERROR}
fi

# #############################################################################
# Send Request
# #############################################################################
LOOP=1
CONTINUE=true
EXIT=0
while [ $CONTINUE = true ]; do
  STATUS=$(curl --output /dev/null --write-out '%{http_code}' --user "${USERNAME}:${PASSWORD}" --data "key=${KEY}&value=${VALUE}" ${ENDPOINT}api/settings/set)

  if [ $STATUS -eq 204 ]; then
    echo "*** Successfully Set $KEY ***"
    CONTINUE=false
  elif [ $LOOP -ne 15 ]; then
    echo "Error: $STATUS returned from service. Sleeping for 60 seconds and then retrying: $LOOP of 10."
    sleep 60
    LOOP=$((LOOP+1))
  else
    echo "Unable to Set $KEY: $STATUS"
    EXIT=$STATUS
    CONTINUE=false
  fi
done

exit $EXIT
