#!/bin/bash

set -e pipefail

# #############################################################################
# Validate Inputs
# #############################################################################
error=0
if [ -z "${USERNAME}" ]; then
  echo "The variable USERNAME must be provided!"
  ERROR=$((${ERROR}+1))
fi

if [ -z "${PASSWORD}" ]; then
  echo "The variable PASSWORD must be provided!"
  ERROR=$((${ERROR}+1))
fi

if [ -z "${ENDPOINT}" ]; then
  echo "The variable ENDPOINT must be provided!"
  ERROR=$((${ENDPOINT}+1))
fi

if [ -z "${GROUP_NAME}" ]; then
  echo "The variable GROUP_NAME must be provided!"
  ERROR=$((${ERROR}+1))
fi

if [ -z "${PERMISSION_NAME}" ]; then
  echo "The variable PERMISSION_NAME must be provided!"
  ERROR=$((${ERROR}+1))
fi

if (( ERROR != 0 )); then
  exit ${ERROR}
fi

# #############################################################################
# Send Request
# #############################################################################
LOOP=1
CONTINUE=true
EXIT=0
while [ $CONTINUE = true ]; do
  STATUS=$(curl --output /dev/null --write-out '%{http_code}' --user "${USERNAME}:${PASSWORD}" --data "groupName=${GROUP_NAME}&permission=${PERMISSION_NAME}" ${ENDPOINT}api/permissions/add_group)

  if [ $STATUS -eq 204 ]; then
    echo "*** Successfully Created Group ***"
    CONTINUE=false
  elif [ $LOOP -ne 15 ]; then
    echo "Error: $STATUS returned from service. Sleeping for 60 seconds and then retrying: $LOOP of 10."
    sleep 60
    LOOP=$((LOOP+1))
  else
    echo "Unable to Create the Requested Group: $STATUS"
    EXIT=$STATUS
    CONTINUE=false
  fi
done

exit $EXIT
