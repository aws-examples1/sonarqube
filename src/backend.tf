terraform {
  backend "s3" {
    bucket         = "gafg-shared-cloudops-terraform-us-east-1"
    key            = "cloudops-sonarqube-orchestration"
    region         = "us-east-1"
    role_arn       = "arn:aws:iam::372892429066:role/gitlabEcsInstanceRole"
    dynamodb_table = "terraform-statelock"
    encrypt        = true
    kms_key_id     = "arn:aws:kms:us-east-1:013163831747:alias/TerraformBootstrap"
  }
}
