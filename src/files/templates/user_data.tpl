#!/bin/bash

set -eou pipefail

# #############################################################################
# Install AWS CLI and Dependencies Required to Use Effectively
# #############################################################################
yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
sudo yum install -y jq unzip
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "/tmp/awscliv2.zip"
unzip /tmp/awscliv2.zip -d /tmp
sudo /tmp/aws/install
rm /tmp/awscliv2.zip
rm -rf /tmp/aws

# #############################################################################
# Setup Autosign Grain Required to Join Salt Master
# #############################################################################
INSTANCE_ID=$(curl http://169.254.169.254/latest/meta-data/instance-id 2>/dev/null)
RESPONSE=$(/usr/local/bin/aws --region ${aws_region} lambda invoke --function-name ${salt_uuid_function} --cli-binary-format raw-in-base64-out --payload "{ \"account_alias\": \"${aws_account_alias}\", \"instance_id\": \"$INSTANCE_ID\" }" response.json)
STATUS=$(echo $RESPONSE | jq --raw-output '.StatusCode')
FUNCTION_ERROR=$(echo $RESPONSE | jq --raw-output '.FunctionError')
if [ -z "$FUNCTION_ERROR" ] || [ $STATUS != 200 ]; then
  ERROR_MESSAGE=$(cat response.json | jq --raw-output '.errorMessage')
  /usr/local/bin/aws sns publish --target-arn ${sns_arn} --message "Error returned from ${salt_uuid_function}: StatusCode $STATUS, FunctionError $FUNCTION_ERROR, errorMessage $ERROR_MESSAGE"
  exit 1
else
  mkdir -p /etc/salt
  UUID=$(cat response.json | jq --raw-output '.')
  echo "uuid: $UUID" > /etc/salt/grains
  echo "roles:" >> /etc/salt/grains
  echo "  - ${salt_role}" >> /etc/salt/grains
fi

echo "autosign_grains:" >> /etc/salt/minion
echo "  - uuid" >> /etc/salt/minion
echo "startup_states: highstate" >> /etc/salt/minion

# #############################################################################
# Stage Database Connectivity Information for Salt
#
# Note: We need to find a better strategy to share credentials across the
# Terraform database provisioning process and the Salt application installation
# and configuration process.
# #############################################################################
cat << EOF > /root/sonar.properties
sonar.jdbc.username=${database_username}
sonar.jdbc.password=${database_password}
sonar.jdbc.url=${database_url}
EOF

# #############################################################################
# Install Salt Minion and Join Salt Master
# #############################################################################
curl -o /tmp/bootstrap-salt.sh -L https://bootstrap.saltstack.com
chmod +x /tmp/bootstrap-salt.sh
/tmp/bootstrap-salt.sh -A "${saltmaster}" -i "${salt_minion_id}"

while [ ! -f ${cloud_init_complete_filename} ]; do
  echo "${cloud_init_complete_filename} doesn't exist yet. Sleeping for 15 seconds before re-checking."
  sleep 15
done

# #############################################################################
# Update Autosign Grain Entry to Indicate Initial Highstate Complete
# #############################################################################
RESPONSE=$(/usr/local/bin/aws --region ${aws_region} lambda invoke --function-name ${salt_uuid_update_function} --cli-binary-format raw-in-base64-out --payload "{ \"instance_id\": \"$INSTANCE_ID\" }" response.json)
STATUS=$(echo $RESPONSE | jq --raw-output '.StatusCode')
FUNCTION_ERROR=$(echo $RESPONSE | jq --raw-output '.FunctionError')
if [ -z "$STATUS" ] || [ "$STATUS" == *"200"* ]; then
  ERROR_MESSAGE=$(cat response.json | jq --raw-output '.errorMessage')
  /usr/local/bin/aws sns publish --target-arn ${sns_arn} --message "Error returned from ${salt_uuid_update_function}: StatusCode $STATUS, FunctionError $FUNCTION_ERROR, errorMessage $ERROR_MESSAGE"
  exit 1
fi

# #############################################################################
# Restart SonarQube Service
# #############################################################################
systemctl restart sonarqube.service


