output "sonarqube_application_url" {
  value = local.base_url
}

output "sonarqube_database_url" {
  value = aws_rds_cluster.default.endpoint
}
