# SonarQube Orchestration Repository
## Purpose
This project is responsible for deploying SonarQube instances.  SonarQube is an open-source platform developed by SonarSource for continuous inspection of code quality to perform automatic reviews with static analysis of code to detect bugs, code smells, and security vulnerabilities on 20+ programming languages.

## Workflow
This repository leverages feature branches in conjunction with a trunk-based deployment model consisting of a production branch.  As currently defined, the pipeline also supports deployment to a sandbox environment that can be used to test new versions before upgrading production.

Under this model...
* short-lived feature branches are created from the production branch,
* changed as required,
* and then merged back into the production branch for deployment into the shared environment.

## Gitlab
### CI/CD Variables
None

### Pipeline
1. All Terraform files will be validated whenever any branch is updated.
2. If validation succeeds and the update pertains to a deployable branch, then a Terraform plan is created.
3. If a Terraform plan is created and the update pertains to a deployable branch, then the plan created is applied.
4. Applying changes will result in creation/updating of a PostgreSQL RDS serverless cluster, a SonarQube instance, an SSL-enabled load balancer, and a DNS CNAME entry.

## Terraform
### Inputs
| Variable | Description |
| -------- | ----------- |
| TF_VAR_CI_COMMIT_SHORT_SHA  | The first eight characters of the commit revision for which project is built. This value comes directly from the "CI_COMMIT_SHORT_SHA" Gitlab predefined variable. |
| TF_VAR_CI_JOB_ID            | The unique id of the current job that GitLab CI/CD uses internally. This value comes directly from the "CI_JOB_ID" Gitlab predefined variable. |
| TF_VAR_CI_PROJECT_PATH_SLUG | The namespace with project name lowercased with blank spaces replaced by hyphens. This value comes directly from the "CI_PROJECT_PATH_SLUG" Gitlab predefined variable. |
| certificate_arn | An ARN identifying an AWS Certificate Manager SSL certificate associated with the domain identified by the domain variable. This value is set in the gitlab-ci.yml file. |
| database_password | Assigned to SonarQube database during provisioning.  This value is pulled from a Gitlab variable. Will remove and replace with Thycotic or Secrets Manager call in future. |
| domain | Used to retrieve the Route 53 zone where records that need to be resolvable across the AWS and on-premise environments are created. This value is set in the gitlab-ci.yml file. |
| network_account_role | Identifies the IAM role used to access the account where the domain identified by the domain variable is managed. This value is set in the gitlab-ci.yml file. |

### Outputs
| Variable | Description |
| -------- | ----------- |
| sonarqube_application_url | Navigate to this URL to access the SonarQube web application. |
| sonarqube_database_endpoint | This is the endpoint for the PostgreSQL RDS cluster hosting the SonarQube database. |
