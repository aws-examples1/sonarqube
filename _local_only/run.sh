#!/bin/bash

# Ensures that any deployments from the command line include an updated commit tied to any changes that have been made and are being applied.
COMMITTED=$(git status | grep "nothing to commit, working tree clean")
if [ -z "$COMMITTED" ]; then
  echo "Since you haven't committed your code the current command will be overridden with \"validate\"."
  COMMAND=validate
else
  COMMAND=$(echo "$1" | tr '[:upper:]' '[:lower:]')
fi

# Validate Inputs
if [ -z "$COMMAND" ] || ([ "$COMMAND" != "apply" ] &&  [ "$COMMAND" != "destroy" ] && [ "$COMMAND" != "validate" ]); then
  echo "You must pass one of the following arguments to this script: apply, destroy, validate."
  exit 1
fi

# Setup Environment
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
if [ -f "$DIR/env.sh" ]; then
  . $DIR/env.sh
fi

cd $DIR/../src
terraform init

# Set the Terraform workspace to the current deployable branch or default to sandbox.
BRANCH=$(git rev-parse --abbrev-ref HEAD | grep -P "(sandbox|development|qa|production)")
if [ -z "$BRANCH" ]; then
  BRANCH=sandbox
fi
terraform workspace select $BRANCH || terraform workspace new $BRANCH

terraform $COMMAND
